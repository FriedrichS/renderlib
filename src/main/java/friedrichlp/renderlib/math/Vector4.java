/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.math;

import friedrichlp.renderlib.util.Temporary;

public class Vector4 extends Vector3 {
	private static final Vector4 ZERO = new Vector4(0, 0, 0, 0);
	private static final Vector4 ONE = new Vector4(1, 1, 1, 1);
	private static final Temporary<Vector4> tmp = new Temporary<Vector4>(Vector4.class, 10000);

	public static Vector4 lerp(Vector4 a, Vector4 b, float t) {
		Vector4 diff = b.copy().sub(a);
		return a.copy().add(diff.mul(t));
	}

	public static Vector4 ZERO() {
		return ZERO.copy();
	}

	public static Vector4 ONE() {
		return ONE.copy();
	}

	public static Vector4 TEMPORARY() {
		return tmp.get().setZero();
	}

	public static Vector4 TEMPORARY(float x, float y, float z, float w) {
		return tmp.get().set(x, y, z, w);
	}


	public float w;

	public Vector4() {
	}

	public Vector4(float x, float y, float z, float w) {
		super(x, y, z);
		this.w = w;
	}

	public Vector4(double x, double y, double z, double w) {
		super(x, y, z);
		this.w = (float) w;
	}

	public Vector4 add(Vector4 v) {
		return add(v.x, v.y, v.z, v.w);
	}

	public Vector4 add(float f) {
		return add(f, f, f, f);
	}

	public Vector4 add(float x, float y, float z, float w) {
		this.x += x;
		this.y += y;
		this.z += z;
		this.w += w;
		return this;
	}

	public Vector4 sub(Vector4 v) {
		return sub(v.x, v.y, v.z, v.w);
	}

	public Vector4 sub(float f) {
		return sub(f, f, f, f);
	}

	public Vector4 sub(float x, float y, float z, float w) {
		this.x -= x;
		this.y -= y;
		this.z -= z;
		this.w -= w;
		return this;
	}

	public Vector4 mul(Vector4 v) {
		return mul(v.x, v.y, v.z, v.w);
	}

	public Vector4 mul(float f) {
		return mul(f, f, f, f);
	}

	public Vector4 mul(float x, float y, float z, float w) {
		this.x *= x;
		this.y *= y;
		this.z *= z;
		this.w *= w;
		return this;
	}

	public Vector4 div(Vector4 v) {
		return div(v.x, v.y, v.z, v.w);
	}

	public Vector4 div(float f) {
		return div(f, f, f, f);
	}

	public Vector4 div(float x, float y, float z, float w) {
		this.x /= x;
		this.y /= y;
		this.z /= z;
		this.w /= w;
		return this;
	}

	public Vector4 setZero() {
		x = 0;
		y = 0;
		z = 0;
		w = 0;
		return this;
	}

	public boolean isZero() {
		return x == 0 && y == 0 && z == 0 && w == 0;
	}

	public Vector4 set(float x, float y, float z, float w) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		return this;
	}

	public Vector4 set(Vector4 v) {
		return set(v.x, v.y, v.z, v.w);
	}

	public float distanceTo(Vector4 v) {
		float x = v.x - this.x;
		float y = v.y - this.y;
		float z = v.z - this.z;
		float w = v.w - this.w;
		return (float) Math.sqrt(x * x + y * y + z * z + w * w);
	}

	public float magnitude() {
		return (float) Math.sqrt(x * x + y * y + z * z + w * w);
	}

	public float dot(Vector4 v) {
		return x * v.x + y * v.y + z * v.z + w * v.w;
	}

	public Vector4 normalize() {
		return div(magnitude());
	}

	public Vector4 copy() {
		return new Vector4(x, y, z, w);
	}

	@Override
	public String toString() {
		return String.format("Vector4(%s, %s, %s, %s)", x, y, z, w);
	}
}
