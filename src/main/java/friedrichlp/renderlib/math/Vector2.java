/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.math;

import friedrichlp.renderlib.util.Temporary;

public class Vector2 {
	private static final Vector2 ZERO = new Vector2(0, 0);
	private static final Vector2 ONE = new Vector2(1, 1);
	private static final Temporary<Vector2> tmp = new Temporary<Vector2>(Vector2.class, 10000);

	public static Vector2 lerp(Vector2 a, Vector2 b, float t) {
		Vector2 diff = b.copy().sub(a);
		return a.copy().add(diff.mul(t));
	}

	public static Vector2 ZERO() {
		return ZERO.copy();
	}

	public static Vector2 ONE() {
		return ONE.copy();
	}

	public static Vector2 TEMPORARY() {
		return tmp.get().setZero();
	}

	public static Vector2 TEMPORARY(float x, float y) {
		return tmp.get().set(x, y);
	}

	public static Vector2 TEMPORARY(Vector2 v) {
		return tmp.get().set(v);
	}


	public float x;
	public float y;

	public Vector2() {
	}

	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vector2(double x, double y) {
		this.x = (float) x;
		this.y = (float) y;
	}

	public Vector2 add(Vector2 v) {
		return add(v.x, v.y);
	}

	public Vector2 add(float f) {
		return add(f, f);
	}

	public Vector2 add(float x, float y) {
		this.x += x;
		this.y += y;
		return this;
	}

	public Vector2 sub(Vector2 v) {
		return sub(v.x, v.x);
	}

	public Vector2 sub(float f) {
		return sub(f, f);
	}


	public Vector2 sub(float x, float y) {
		this.x -= x;
		this.y -= y;
		return this;
	}

	public Vector2 mul(Vector2 v) {
		return mul(v.x, v.y);
	}

	public Vector2 mul(float f) {
		return mul(f, f);
	}

	public Vector2 mul(float x, float y) {
		this.x *= x;
		this.y *= y;
		return this;
	}

	public Vector2 div(Vector3 v) {
		return div(v.x, v.y);
	}

	public Vector2 div(float f) {
		return div(f, f);
	}

	public Vector2 div(float x, float y) {
		this.x /= x;
		this.y /= y;
		return this;
	}

	public Vector2 setZero() {
		this.x = 0;
		this.y = 0;
		return this;
	}

	public boolean isZero() {
		return this.x == 0 && this.y == 0;
	}

	public Vector2 set(float x, float y) {
		this.x = x;
		this.y = y;
		return this;
	}

	public Vector2 set(Vector2 v) {
		return set(v.x, v.y);
	}

	public float distanceTo(Vector2 v) {
		float x = v.x - this.x;
		float y = v.y - this.y;
		return (float) Math.sqrt(x * x + y * y);
	}

	public float magnitude() {
		return (float) Math.sqrt(x * x + y * y);
	}

	public float dot(Vector2 v) {
		return x * v.x + y * v.y;
	}

	public Vector2 normalize() {
		return div(magnitude());
	}

	public Vector2 copy() {
		return new Vector2(this.x, this.y);
	}

	@Override
	public String toString() {
		return String.format("Vector2(%s, %s)", x, y);
	}
}
