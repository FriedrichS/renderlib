/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib;

import friedrichlp.renderlib.library.LoadingMode;

public class RenderLibSettings {
	public static class General {
		public static LoadingMode LOADING_MODE = LoadingMode.FADE;
		public static float MODEL_UNLOAD_DELAY_MS = 10000;
	}

	public static class Caching {
		public static String CACHE_LOCATION = "/cache/";
		public static String CACHE_VERSION = null;
	}

	public static class Rendering {
		public static float CAMERA_HEIGHT_OFFSET = 0.0f; // used to fix issues regarding lighting
		public static float AMBIENT_LIGHT_STRENGTH = 0.03f;
	}
}
