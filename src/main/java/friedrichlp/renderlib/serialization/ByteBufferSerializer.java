/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.serialization;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteBufferSerializer {
	public static void save(DataOutputStream out, Object o, boolean isDirect) throws IOException, IllegalAccessException {
		ByteBuffer buf = (ByteBuffer)o;
		out.writeInt(buf.capacity());
		for (int i = 0; i < buf.capacity(); i++) {
			out.writeByte(buf.get(i));
		}
	}

	public static ByteBuffer load(DataInputStream in, boolean isDirect) throws IOException, IllegalAccessException {
		int capacity = in.readInt();
		ByteBuffer buf = isDirect ? ByteBuffer.allocateDirect(capacity).order(ByteOrder.nativeOrder()) : ByteBuffer.allocate(capacity);
		for (int i = 0; i < capacity; i++) {
			buf.put(in.readByte());
		}
		buf.flip();
		return buf;
	}
}
