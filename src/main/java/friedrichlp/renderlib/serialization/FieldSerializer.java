/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.serialization;

import friedrichlp.renderlib.caching.util.SerializeField;
import sun.nio.ch.DirectBuffer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Map;

public class FieldSerializer {
	private Object target;
	private DataInputStream in;
	private DataOutputStream out;

	public FieldSerializer(Object target, DataInputStream in) {
		this.target = target;
		this.in = in;
	}

	public FieldSerializer(Object target, DataOutputStream out) {
		this.target = target;
		this.out = out;
	}

	public void save(Field field, SerializeField info) throws IOException, IllegalAccessException {
		Class t = field.getType();
		if (t == boolean.class || t == Boolean.class) {
			out.writeBoolean(field.getBoolean(target));
			if (info.deleteAfterSave()) field.setBoolean(target, false);
		} else if (t == Byte.class || t == byte.class) {
			out.writeByte(field.getByte(target));
			if (info.deleteAfterSave()) field.setByte(target, (byte)0);
		} else if (t == Short.class || t == short.class) {
			out.writeShort(field.getShort(target));
			if (info.deleteAfterSave()) field.setShort(target, (short)0);
		} else if (t == Integer.class || t == int.class) {
			out.writeInt(field.getInt(target));
			if (info.deleteAfterSave()) field.setInt(target, 0);
		} else if (t == Long.class || t == long.class) {
			out.writeLong(field.getLong(target));
			if (info.deleteAfterSave()) field.setLong(target, 0);
		} else if (t == Float.class || t == float.class) {
			out.writeFloat(field.getFloat(target));
			if (info.deleteAfterSave()) field.setFloat(target, 0.0f);
		} else if (t == Double.class || t == double.class) {
			out.writeDouble(field.getDouble(target));
			if (info.deleteAfterSave()) field.setDouble(target, 0.0);
		} else {
			Object o = field.get(target);
			out.writeBoolean(o != null);
			if (o != null) {
				if (t == String.class) {
					out.writeUTF((String)o);
				} else if (Collection.class.isAssignableFrom(t)) {
					CollectionSerializer.save(out, field, o);
				} else if (Map.class.isAssignableFrom(t)) {
					MapSerializer.save(out, field, o);
				} else if (ByteBuffer.class.isAssignableFrom(t)) {
					if (DirectBuffer.class.isAssignableFrom(t)) {
						ByteBufferSerializer.save(out, o, true);
					} else {
						ByteBufferSerializer.save(out, o, false);
					}
				} else {
					Serializer.save(out, o);
				}

				if (info.deleteAfterSave()) field.set(target, null);
			}
		}
	}

	public void load(Field field, SerializeField info) throws IOException, IllegalAccessException {
		Class t = field.getType();
		if (t == Byte.class || t == byte.class) {
			field.setByte(target, in.readByte());
		} else if (t == Short.class || t == short.class) {
			field.setShort(target, in.readShort());
		} else if (t == Integer.class || t == int.class) {
			field.setInt(target, in.readInt());
		} else if (t == Long.class || t == long.class) {
			field.setLong(target, in.readLong());
		} else if (t == Float.class || t == float.class) {
			field.setFloat(target, in.readFloat());
		} else if (t == Double.class || t == double.class) {
			field.setDouble(target, in.readDouble());
		} else {
			boolean isNotNull = in.readBoolean();
			if (isNotNull) {
				Object o = null;
				try {
					o = t.newInstance();
				} catch (InstantiationException e) {}

				if (t == String.class) {
					o = in.readUTF();
				} else if(Collection.class.isAssignableFrom(t)) {
					CollectionSerializer.load(in, field, o);
				} else if (Map.class.isAssignableFrom(t)) {
					MapSerializer.load(in, field, o);
				} else if (ByteBuffer.class.isAssignableFrom(t)) {
					if (DirectBuffer.class.isAssignableFrom(t)) {
						o = ByteBufferSerializer.load(in, true);
					} else {
						o = ByteBufferSerializer.load(in, false);
					}
				} else {
					o = Serializer.load(in, null, t);
				}

				field.set(target, o);
			}
		}
	}
}
