/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.serialization;

import friedrichlp.renderlib.caching.util.SerializeField;
import friedrichlp.renderlib.util.ConsoleLogger;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;

public class Serializer {
	private static Object2ObjectArrayMap<Class, FieldStorage> storages = new Object2ObjectArrayMap<Class, FieldStorage>();

	public static void save(DataOutputStream out, Object o) throws IOException {
		FieldStorage storage = storages.get(o.getClass());
		if (storage == null) {
			storage = new FieldStorage(o.getClass());
			storages.put(o.getClass(), storage);
		}

		try {
			storage.save(out, new FieldSerializer(o, out));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static Object load(DataInputStream in, Object o, Class<?> cls) throws IOException {
		FieldStorage storage = storages.get(cls);
		if (storage == null) {
			storage = new FieldStorage(cls);
			storages.put(cls, storage);
		}

		if (o == null) {
			try {
				o = cls.newInstance();
			} catch (Exception e) {
				ConsoleLogger.error("FATAL: Serialized class %s needs to have default constructor", cls.getName());
			}
		}
		try {
			storage.load(in, new FieldSerializer(o, in));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return o;
	}

	private static class FieldStorage {
		private Object2ObjectArrayMap<String, FieldEntry> fields = new Object2ObjectArrayMap<String, FieldEntry>();

		private FieldStorage(Class cls) {
			for (Field field : cls.getDeclaredFields()) {
				SerializeField f = field.getAnnotation(SerializeField.class);
				if (f != null) {
					String name = field.getName();
					if (!f.name().equals("")) {
						name = f.name();
					}
					field.setAccessible(true);
					fields.put(name, new FieldEntry(field, f));
				}
			}
		}

		private void save(DataOutputStream out, FieldSerializer fs) throws IOException, IllegalAccessException {
			for (Map.Entry<String, FieldEntry> e : fields.entrySet()) {
				out.writeUTF(e.getKey());

				FieldEntry fe = e.getValue();
				fs.save(fe.field, fe.info);
			}
		}

		private void load(DataInputStream in, FieldSerializer fs) throws IOException, IllegalAccessException {
			for (int i = 0; i < fields.size(); i++) {
				String fieldName = in.readUTF();
				FieldEntry e = fields.get(fieldName);
				fs.load(e.field, e.info);
			}
		}

		private static class FieldEntry {
			private Field field;
			private SerializeField info;

			private FieldEntry(Field field, SerializeField info) {
				this.field = field;
				this.info = info;
			}
		}
	}
}
