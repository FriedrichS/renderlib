/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.animation.loader;

import friedrichlp.renderlib.animation.Animation;
import friedrichlp.renderlib.animation.Keyframe;
import friedrichlp.renderlib.math.Vector3;
import friedrichlp.renderlib.util.IndexList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class AnimationLoader {
	public static void loadAnimation(File file, Animation anim) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));

		String currentObject = "null";
		IndexList<Keyframe> keyframes = new IndexList<Keyframe>();
		float time = 0;
		Vector3 loc = null;
		Vector3 rot = null;
		Vector3 scale = null;
		Vector3 origin = null;

		String line;
		while ((line = reader.readLine()) != null) {
			if (line.startsWith("#")) continue;
			if (line.length() == 0) continue;
			String[] args = line.split(" ");
			switch (args[0].charAt(0)) {
				case 'o':
					if (currentObject != "null") {
						anim.addKeyframes(currentObject, keyframes);
						anim.setObjectOrigin(currentObject, origin);
					}
					keyframes.clear();
					currentObject = args[1];
					break;
				case 't':
					if (loc != null) {
						keyframes.add(new Keyframe(time, loc, rot, scale));
					}
					time = Float.parseFloat(args[1]);
					break;
				case 'l':
					Vector3 v = new Vector3(Float.parseFloat(args[1]), Float.parseFloat(args[3]), Float.parseFloat(args[2]));
					if (keyframes.size() == 0) origin = v.copy();
					loc = v.sub(origin);
					break;
				case 'r':
					rot = new Vector3(Float.parseFloat(args[1]), Float.parseFloat(args[3]), Float.parseFloat(args[2]));
					break;
				case 's':
					scale = new Vector3(Float.parseFloat(args[1]), Float.parseFloat(args[3]), Float.parseFloat(args[2]));
					break;
			}
		}

		keyframes.add(new Keyframe(time, loc, rot, scale));
		anim.addKeyframes(currentObject, keyframes);
		anim.setObjectOrigin(currentObject, origin);

		reader.close();
	}
}
