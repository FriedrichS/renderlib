/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.render;

import friedrichlp.renderlib.RenderLibRegistry;
import friedrichlp.renderlib.library.GLValueType;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class GLState {
	private ObjectArrayList<StateValueContainer> stateValues = new ObjectArrayList<StateValueContainer>();
	private boolean[] vertexAttribArrays;
	private boolean saveVertexAttribState = false;

	public GLState add(GLValueType type) {
		stateValues.add(new StateValueContainer(type));

		return this;
	}

	public GLState saveVertexAttribState() {
		saveVertexAttribState = true;
		vertexAttribArrays = new boolean[GL11.glGetInteger(GL20.GL_MAX_VERTEX_ATTRIBS)];

		return this;
	}

	public void save() {
		for (StateValueContainer c : stateValues) {
			c.value = c.type.get();
		}

		if (saveVertexAttribState) {
			for (int i = 0; i < vertexAttribArrays.length; i++) {
				vertexAttribArrays[i] = RenderLibRegistry.Compatibility.GL_GET_VERTEX_ATTRIB.apply(i, GL20.GL_VERTEX_ATTRIB_ARRAY_ENABLED) == 0 ? false : true;
			}
		}
	}

	public void restore() {
		for (StateValueContainer c : stateValues) {
			c.type.set(c.value);
		}

		if (saveVertexAttribState) {
			for (int i = 0; i < vertexAttribArrays.length; i++) {
				if (vertexAttribArrays[i]) {
					GL20.glEnableVertexAttribArray(i);
				} else {
					GL20.glDisableVertexAttribArray(i);
				}
			}
		}
	}

	private static class StateValueContainer {
		private GLValueType type;
		private int value;

		private StateValueContainer(GLValueType type) {
			this.type = type;
		}
	}
}
