/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.render;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShaderPreprocessor {
	private static final TokenDefinition[] tokenDefs = new TokenDefinition[] {
			token("#version \\d+"),
			token("#define ([^ \\n]+)\\n"), // define [var]
			token("#define (.+ .+)"), // define [var] [val]
			token("#ifdef ([^ \\n]+)"),
			token("#else"),
			token("#endif"),
			token("#extension .+"),
			token(".*[;{}(),]"), // any line of code
	};

	public static String process(String shader) {
		// tokenize the string
		ObjectArrayList<Token> tokens = new ObjectArrayList<Token>();
		String s = shader;
		while (true) {
			Matcher firstMatch = null;
			int matchId = -1;
			for (TokenDefinition t : tokenDefs) {
				Matcher m = t.regex.matcher(s);
				if (m.find() && (firstMatch == null || firstMatch.start() > m.start())) {
					firstMatch = m;
					matchId = t.id;
				}
			}
			if (firstMatch == null) break;

			MatchResult m = firstMatch.toMatchResult();
			tokens.add(new Token(m, matchId));
			s = s.substring(m.end());
		}

		// process tokens and reconstruct string
		String processedString = "";
		ObjectArrayList<String> defines = new ObjectArrayList<String>();
		boolean[] stack = new boolean[32];
		int stackPtr = 0;
		stack[stackPtr] = true;
		for (Token t : tokens) {
			boolean skipCurrentToken = true;
			if (t.id == 1) {
				defines.add(t.match.group(1));
			} else if (t.id == 3) { // #ifdef
				boolean val = false;
				if (stack[stackPtr]) {
					val = defines.contains(t.match.group(1));
				}
				stack[++stackPtr] = val;
			} else if (t.id == 4) { // #else
				if (stack[stackPtr - 1]) {
					stack[stackPtr] = !stack[stackPtr];
				}
			} else if (t.id == 5) { // #endif
				stackPtr--;
			} else {
				skipCurrentToken = false;
			}

			if (stack[stackPtr] && !skipCurrentToken) {
				processedString += t.match.group() + "\n";
			}
		}

		//System.out.println(processedString);
		return processedString;
	}

	private static int currentTokenIndex = 0;
	private static TokenDefinition token(String pattern) {
		return new TokenDefinition(pattern, currentTokenIndex++);
	}

	private static class Token {
		private MatchResult match;
		private int id;

		private Token(MatchResult match, int id) {
			this.match = match;
			this.id = id;
		}
	}

	private static class TokenDefinition {
		private Pattern regex;
		private int id;

		private TokenDefinition(String pattern, int id) {
			regex = Pattern.compile(pattern);
			this.id = id;
		}
	}
}
