/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import java.util.concurrent.atomic.AtomicLong;

public class Temporary<T> {
	public final Object[] buf;
	private AtomicLong idx = new AtomicLong(0);
	private int size;

	public Temporary(Class<?> clazz, int size) {
		buf = new Object[size];
		try {
			for (int i = 0; i < size; i++) buf[i] = clazz.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.size = size;
	}

	public T get() {
		return (T) buf[(int) (idx.getAndIncrement() % size)];
	}
}
