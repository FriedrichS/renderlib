/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class IndexList<T> {
	private ObjectArrayList<T> items = new ObjectArrayList<T>();

	public void add(T item) {
		items.add(item);
	}

	public void remove(T item) {
		items.remove(item);
	}

	public T next(Index index, boolean apply) {
		if (apply) {
			index.index++;
			if (index.index >= items.size() - 1) index.index = 0;
			return items.get(index.index);
		}
		return items.get(index.getNext(items.size()));
	}

	public T get(int index) {
		return items.get(index);
	}

	public T get(Index index) {
		return items.get(index.index);
	}

	public void clear() {
		items.clear();
	}

	public int size() {
		return items.size();
	}

	public void incrementIndex(Index index) {
		index.increment(items.size());
	}

	public static class Index {
		public int index = 0;

		public void increment(int limit) {
			if (index >= limit - 1) index = 0;
			else index++;
		}

		public int getNext(int limit) {
			if (index >= limit - 1) return 0;
			return index + 1;
		}
	}
}
