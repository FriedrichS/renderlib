/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

public class ConsoleLogger {
	private static final String RESET = "\u001B[0m";
	private static final String RED = "\u001B[31m";
	private static final String GREEN = "\u001B[32m";
	private static final String YELLOW = "\u001B[33m";
	private static boolean colorLogging = true;
	private static boolean debugLogging = true;
	private static boolean consoleLogging = true;
	private static boolean additionalDebugInfo = true;

	public static void disableColorLogging() {
		colorLogging = false;
	}

	public static void enableColorLogging() {
		colorLogging = true;
	}

	public static void disableLogging() {
		consoleLogging = false;
	}

	public static void enableLogging() {
		consoleLogging = true;
	}

	public static void disableDebugLogging() {
		debugLogging = false;
	}

	public static void enableDebugLogging() {
		debugLogging = true;
	}

	public static void disableAdditionalDebugInfo() {
		additionalDebugInfo = false;
	}

	public static void enableAdditionalDebugInfo() {
		additionalDebugInfo = true;
	}

	public static void info(String msg, Object... args) {
		String s = extractInvokerInfo(new Throwable().getStackTrace(), false);
		if (!consoleLogging) return;
		if (args != null) msg = String.format(msg, args);
		msg = "[RENDERLIB-INFO] " + msg;
		System.out.println(msg);
		if (additionalDebugInfo) System.out.println("At: " + s);
	}

	public static void warn(String msg, Object... args) {
		String s = extractInvokerInfo(new Throwable().getStackTrace(), false);
		if (!consoleLogging) return;
		if (args != null) msg = String.format(msg, args);
		if (colorLogging) msg = YELLOW + "[RENDERLIB-WARN] " + RESET + msg;
		else msg = "[RENDERLIB-WARN] " + msg;
		System.out.println(msg);
		if (additionalDebugInfo) System.out.println("At: " + s);
	}

	public static void error(String msg, Object... args) {
		String s = extractInvokerInfo(new Throwable().getStackTrace(), false);
		if (!consoleLogging) return;
		if (args != null) msg = String.format(msg, args);
		if (colorLogging) msg = RED + "[RENDERLIB-ERROR] " + RESET + msg;
		else msg = "[RENDERLIB-ERROR] " + msg;
		System.out.println(msg);
		if (additionalDebugInfo) System.out.println("At: " + s);
	}

	public static void debug(String msg, Object... args) {
		String s = extractInvokerInfo(new Throwable().getStackTrace(), false);
		if (!consoleLogging || !debugLogging) return;
		if (args != null) msg = String.format(msg, args);
		if (colorLogging) msg = GREEN + "[RENDERLIB-DEBUG] " + RESET + msg;
		else msg = "[RENDERLIB-DEBUG] " + msg;
		System.out.println(msg);
		if (additionalDebugInfo) System.out.println("At: " + s);
	}

	public static String extractInvokerInfo(StackTraceElement[] stackTrace, boolean addLineNumber) {
		String clsName = stackTrace[1].getClassName();
		if (addLineNumber) {
			return String.format("%s.%s(%s:%s)", clsName, stackTrace[1].getMethodName(), clsName.substring(clsName.lastIndexOf('.') + 1), stackTrace[1].getLineNumber());
		}
		return String.format("%s.%s()", clsName, stackTrace[1].getMethodName());
	}
}
