/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import friedrichlp.renderlib.RenderLibSettings;
import friedrichlp.renderlib.tracking.TrackingManager;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public abstract class DynamicLoader {
	private static ObjectArrayList<DynamicLoader> instances = new ObjectArrayList<DynamicLoader>();
	private static long lastMs = -1;

	private float msUnused;
	private boolean isLoaded;

	public DynamicLoader() {
		instances.add(this);
	}

	public boolean isLoaded() {
		return isLoaded;
	}

	public void onUse() {
		msUnused = 0;
	}

	public void destroy() {
		instances.remove(this);
	}

	public void load() {
		if (isLoaded) return;
		isLoaded = true;
		msUnused = 0;
		onLoad();
	}

	public void unload() {
		if (!isLoaded) return;
		isLoaded = false;
		onUnload();
	}

	public abstract void onLoad();

	public abstract void onUnload();

	public static void update() {
		if (lastMs < 0) {
			lastMs = System.currentTimeMillis();
		}
		long ms = System.currentTimeMillis() - lastMs;
		lastMs = System.currentTimeMillis();

		for (DynamicLoader u : instances) {
			if (!u.isLoaded) continue;

			u.msUnused += ms;
			if (u.msUnused >= RenderLibSettings.General.MODEL_UNLOAD_DELAY_MS) {
				u.unload();
			}
		}
	}
}
