/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.library;

import friedrichlp.renderlib.oglw.GLDrawSetup;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

public enum TextureType {
	MAP_KD(
			"map_Kd",
			0,
			(idx, cellSize) -> {}),
	MAP_BUMP(
			"map_Bump",
			8,
			(idx, cellSize) -> {
				GLDrawSetup.vertexAttribPointer(8, 4, GLDrawSetup.Type.INT_2_10_10_10_REV, true, cellSize, idx.getAndAdd(4));
				GLDrawSetup.vertexAttribPointer(9, 4, GLDrawSetup.Type.INT_2_10_10_10_REV, true, cellSize, idx.getAndAdd(4));
			}),
	REFL(
			"refl",
			0,
			(idx, cellSize) -> {});

	String identifier;
	int size;
	BiConsumer<AtomicInteger, Integer> setupFunction;
	TextureType(String identifier, int size, BiConsumer<AtomicInteger, Integer> setupFunction) {
		this.identifier = identifier;
		this.size = size;
		this.setupFunction = setupFunction;
	}

	public void renderSetup(AtomicInteger idx, int cellSize) {
		setupFunction.accept(idx, cellSize);
	}

	public void renderCleanup() {

	}

	public int getSize() {
		return size;
	}

	public boolean isActive(int code) {
		return (code & (1 << ordinal())) != 0;
	}

	public static TextureType fromIdentifier(String identifier) {
		for (TextureType t : values()) {
			if (t.identifier.equals(identifier)) {
				return t;
			}
		}
		return null;
	}
}
