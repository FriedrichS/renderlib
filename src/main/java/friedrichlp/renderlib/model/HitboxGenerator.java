/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.model;

import friedrichlp.renderlib.math.HitBox3;
import friedrichlp.renderlib.tracking.Model;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;

public class HitboxGenerator {
	public static HitBox3 generateHitbox(Model model, FloatArrayList verts, IntArrayList faceVerts) {
		HitBox3 hitbox = new HitBox3();

		for (Model.Group group : model.data.groups.values()) {
			System.out.println(group.start + " " + (group.start + group.length));
		}

		for (Model.Group group : model.data.groups.values()) {
			HitBox3 box = new HitBox3();

			int start = group.start * 3;
			int end = start + group.length * 3;
			for (int i = start; i < end; i += 3) {
				float x = verts.getFloat(i);
				float y = verts.getFloat(i + 1);
				float z = verts.getFloat(i + 2);

				box.minX = Math.min(box.minX, x);
				box.minY = Math.min(box.minY, y);
				box.minZ = Math.min(box.minZ, z);
				box.maxX = Math.max(box.maxX, x);
				box.maxY = Math.max(box.maxY, y);
				box.maxZ = Math.max(box.maxZ, z);
			}

			hitbox.combine(box);
			group.hitbox = box;
		}

		return hitbox;
	}
}
