/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.model;

import friedrichlp.renderlib.RenderLibRegistry;
import friedrichlp.renderlib.caching.CacheManager;
import friedrichlp.renderlib.caching.Cacheable;
import friedrichlp.renderlib.caching.ICacheData;
import friedrichlp.renderlib.caching.util.DataSerializer;
import friedrichlp.renderlib.caching.util.HashedFile;
import friedrichlp.renderlib.caching.util.SerializeField;
import friedrichlp.renderlib.math.HitBox3;
import friedrichlp.renderlib.tracking.Model;
import friedrichlp.renderlib.util.BufferUtil;
import friedrichlp.renderlib.util.IFileContainer;
import friedrichlp.renderlib.util.UnsafeUtil;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.lwjgl.BufferUtils;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.Map;

public class ModelData extends Cacheable {
	private static final int HASHED_BLOCK_SIZE = 64000;

	public boolean needsDataRefresh;

	@SerializeField
	public int vboSize;
	@SerializeField
	public HitBox3 hitbox;

	public ByteBuffer modelBuf;
	public ByteBuffer mtlIdxBuf;
	public ByteBuffer textureMtlBuf;

	@SerializeField
	public Object2ObjectArrayMap<String, Model.Group> groups = new Object2ObjectArrayMap<>();
	public ObjectArrayList<IFileContainer> mtls = new ObjectArrayList<>();
	public Int2ObjectArrayMap<CachedTexture> textures = new Int2ObjectArrayMap<>();
	@SerializeField
	public int loadedTextureTypes;

	public File modelCacheFile;
	public File textureCacheFile;

	private ObjectArrayList<HashedFile> hashedFiles = new ObjectArrayList<>();
	private Object2ObjectArrayMap<String, ICacheData> additionalData;
	private ObjectArrayList<IFileContainer> additionalHashedFiles = new ObjectArrayList<>();
	private Model parent;

	public ModelData(Model parent) {
		this.parent = parent;
		additionalData = parent.properties.additionalData;
		setName(parent.identifier);

		modelCacheFile = CacheManager.getCacheFile(String.format("%s-mdl.cache", getNameHashString()));
		textureCacheFile = CacheManager.getCacheFile(String.format("%s-tex.cache", getNameHashString()));
	}

	@Override
	public void onSave(DataOutputStream out) throws IOException {
		DataSerializer ds = new DataSerializer(out);

		out.writeInt(hashedFiles.size());
		for (HashedFile f : hashedFiles) {
			ds.writeHashedFile(f);
		}

		out.writeInt(additionalData.size());
		for (Map.Entry<String, ICacheData> e : additionalData.entrySet()) {
			out.writeUTF(e.getKey());
			e.getValue().save(out);
		}
	}

	@Override
	public void onLoad(DataInputStream in) throws IOException {
		DataSerializer ds = new DataSerializer(in);

		int count = in.readInt();
		for (int i = 0; i < count; i++) {
			hashedFiles.add(ds.readHashedFile());
		}

		count = in.readInt();
		for (int i = 0; i < count; i++) {
			String clsName = in.readUTF();
			additionalData.get(clsName).load(in);
		}
	}

	public void writeCache() {
		try (DataOutputStream out = new DataOutputStream(new SnappyOutputStream(new FileOutputStream(modelCacheFile)))) {
			BufferUtil.write(out, modelBuf);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (DataOutputStream out = new DataOutputStream(new SnappyOutputStream(new FileOutputStream(textureCacheFile)))) {
			out.writeInt(textures.size());
			for (Int2ObjectMap.Entry<CachedTexture> e : textures.int2ObjectEntrySet()) {
				out.writeInt(e.getIntKey());
				out.writeInt(e.getValue().size);
				BufferUtil.write(out, e.getValue().buf);
			}

			BufferUtil.write(out, mtlIdxBuf);
			BufferUtil.write(out, textureMtlBuf);
		} catch (IOException e) {
			e.printStackTrace();
		}

		clearBuffers();
	}

	public void readCache() {
		try (DataInputStream in = new DataInputStream(new SnappyInputStream(new FileInputStream(modelCacheFile)))) {
			int length = in.readInt();
			modelBuf = BufferUtils.createByteBuffer(length);
			BufferUtil.read(in, modelBuf);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (DataInputStream in = new DataInputStream(new SnappyInputStream(new FileInputStream(textureCacheFile)))) {
			int size = in.readInt();
			for (int i = 0; i < size; i++) {
				int type = in.readInt();
				int s = in.readInt();
				int capacity = in.readInt();
				ByteBuffer buf = BufferUtils.createByteBuffer(capacity);
				BufferUtil.read(in, buf);

				textures.put(type, new CachedTexture(s, buf));
			}

			int length = in.readInt();
			mtlIdxBuf = BufferUtils.createByteBuffer(length);
			BufferUtil.read(in, mtlIdxBuf);

			length = in.readInt();
			textureMtlBuf = BufferUtils.createByteBuffer(length);
			BufferUtil.read(in, textureMtlBuf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void awaitDataRefresh() {
		needsDataRefresh = true;

		groups.clear();
		mtls.clear();
		textures.clear();
	}

	public boolean hasChanged() {
		if (hashedFiles.isEmpty()) {
			updateHashedFiles();
			return true;
		}

		for (HashedFile f : hashedFiles) {
			HashedFile current = new HashedFile(f);
			if (!f.isEqualTo(current)) return true;
		}
		return false;
	}

	public void updateHashedFiles() {
		hashedFiles.clear();

		hashedFiles.add(new HashedFile(parent.getFile(), HASHED_BLOCK_SIZE));

		for (IFileContainer mtl : mtls) {
			hashedFiles.add(new HashedFile(mtl, HASHED_BLOCK_SIZE));

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(mtl.getStream()))) {
				String line;
				while ((line = reader.readLine()) != null) {
					if (line.startsWith("map")) {
						hashedFiles.add(new HashedFile(parent.getFile().getRelative(line.split(" ")[1]), HASHED_BLOCK_SIZE));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		for (IFileContainer fc : additionalHashedFiles) {
			hashedFiles.add(new HashedFile(fc, HASHED_BLOCK_SIZE));
		}
	}

	public void clearBuffers() {
		needsDataRefresh = false;

		modelBuf = null;
		mtlIdxBuf = null;
		textureMtlBuf = null;
	}

	public ICacheData getAdditionalCacheData(Class<? extends ICacheData> clazz) {
		return additionalData.get(clazz.getName());
	}

	public void addAdditionalHashedFile(IFileContainer data) {
		additionalHashedFiles.add(data);
	}

	public void updateAdditionalCacheData() {
		additionalData.values().forEach(ICacheData::reload);
	}

	public static class CachedTexture {
		public int size;
		public ByteBuffer buf;

		public CachedTexture() {}

		public CachedTexture(int size, ByteBuffer buf) {
			this.size = size;
			this.buf = buf;
		}
	}
}
