/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.oglw;

import friedrichlp.renderlib.library.GLException;
import friedrichlp.renderlib.oglw.state.Texture;
import org.lwjgl.opengl.*;

import java.nio.ByteBuffer;

public class GLTextures {
	public enum Target {
		TEXTURE_1D(GL11.GL_TEXTURE_1D),
		TEXTURE_2D(GL11.GL_TEXTURE_2D),
		TEXTURE_3D(GL12.GL_TEXTURE_3D),
		TEXTURE_1D_ARRAY(GL30.GL_TEXTURE_1D_ARRAY),
		TEXTURE_2D_ARRAY(GL30.GL_TEXTURE_2D_ARRAY),
		TEXTURE_RECTANGLE(GL31.GL_TEXTURE_RECTANGLE),
		TEXTURE_CUBE_MAP(GL13.GL_TEXTURE_CUBE_MAP),
		TEXTURE_BUFFER(GL31.GL_TEXTURE_BUFFER)
		;

		private int val;
		Target(int val) {
			this.val = val;
		}
	}

	public enum Format {
		RGBA(GL11.GL_RGBA),
		BGRA(GL12.GL_BGRA)
		;

		private int val;
		Format(int val) {
			this.val = val;
		}
	}

	public enum InternalFormat {
		SRGB_ALPHA(GL21.GL_SRGB_ALPHA)
		;

		private int val;
		InternalFormat(int val) {
			this.val = val;
		}
	}

	public enum Type {
		UNSIGNED_BYTE(GL11.GL_UNSIGNED_BYTE)
		;

		private int val;
		Type(int val) {
			this.val = val;
		}
	}

	public enum ParamName {
		TEXTURE_MAG_FILTER(GL11.GL_TEXTURE_MAG_FILTER),
		TEXTURE_MIN_FILTER(GL11.GL_TEXTURE_MIN_FILTER),
		TEXTURE_WRAP_S(GL11.GL_TEXTURE_WRAP_S),
		TEXTURE_WRAP_T(GL11.GL_TEXTURE_WRAP_T)
		;

		private int val;
		ParamName(int val) {
			this.val = val;
		}
	}

	public enum ParamValue {
		NEAREST(GL11.GL_NEAREST),
		LINEAR(GL11.GL_LINEAR),
		CLAMP_TO_EDGE(GL12.GL_CLAMP_TO_EDGE)
		;

		private int val;
		ParamValue(int val) {
			this.val = val;
		}
	}

	public static int genTexture() {
		return GL11.glGenTextures();
	}

	public static void deleteTexture(int texture) {
		GL11.glDeleteTextures(texture);
	}

	public static void bind(Target target, int texture) {
		if (GLWrapperSettings.doChecks) {
			if (texture == 0) {
				ExceptionHandler.handle(GLException.WARNING, "tried to bind 0 to texture");
				// we don't return here because this is technically not an error
			}
		}

		Texture.bind(target.val, texture);
	}

	public static void bind(Target target, int texture, int slot) {
		if (GLWrapperSettings.doChecks) {
			if (texture == 0) {
				ExceptionHandler.handle(GLException.WARNING, "tried to bind 0 to texture");
				// we don't return here because this is technically not an error
			}
		}

		Texture.bind(target.val, texture, slot);
	}

	public static void setSubImage2D(Target target, int level, int xOffset, int yOffset, int width, int height, Format format, Type type, int pixels) {
		if (GLWrapperSettings.doChecks) {
			if (level < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "level (%s) was less than 0", level);
				return;
			} else if (width < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "width (%s) was less than 0", width);
				return;
			} else if (height < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "height (%s) was less than 0", height);
				return;
			}
		}

		GL11.glTexSubImage2D(target.val, level, xOffset, yOffset, width, height, format.val, type.val, pixels);
	}

	public static void setSubImage2D(Target target, int level, int xOffset, int yOffset, int width, int height, Format format, Type type, ByteBuffer pixels) {
		if (GLWrapperSettings.doChecks) {
			if (level < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "level (%s) was less than 0", level);
				return;
			} else if (width < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "width (%s) was less than 0", width);
				return;
			} else if (height < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "height (%s) was less than 0", height);
				return;
			}
		}

		GL11.glTexSubImage2D(target.val, level, xOffset, yOffset, width, height, format.val, type.val, pixels);
	}

	public static void setImage2D(Target target, int level, InternalFormat internalFormat, int width, int height, Format format, Type type, ByteBuffer data) {
		if (GLWrapperSettings.doChecks) {
			if (level < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "level (%s) was less than 0", level);
				return;
			} else if (width < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "width (%s) was less than 0", width);
				return;
			} else if (height < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "height (%s) was less than 0", height);
				return;
			}
		}

		GL11.glTexImage2D(target.val, level, internalFormat.val, width, height, 0, format.val, type.val, data);
	}

	public static void setParameter(Target target, ParamName pname, ParamValue param) {
		if (GLWrapperSettings.doChecks) {

		}

		GL11.glTexParameteri(target.val, pname.val, param.val);
	}

	public static void getImage(Target target, int level, Format format, Type type, ByteBuffer pixels) {
		if (GLWrapperSettings.doChecks) {
			if (level < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "level (%s) was less than 0", level);
				return;
			}
		}

		GL11.glGetTexImage(target.val, 0, format.val, type.val, pixels);
	}
}
