/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.oglw;

import friedrichlp.renderlib.library.GLException;
import friedrichlp.renderlib.oglw.state.Buffer;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL31;

import java.nio.ByteBuffer;

public class GLBuffers {
	public enum Target {
		VERTEX_ARRAY(GL15.GL_ARRAY_BUFFER),
		UNIFORM_BUFFER(GL31.GL_UNIFORM_BUFFER),
		PIXEL_UNPACK_BUFFER(GL21.GL_PIXEL_UNPACK_BUFFER)
		;

		private int val;
		Target(int val) {
			this.val = val;
		}
	}

	public enum Usage {
			STATIC_DRAW(GL15.GL_STATIC_DRAW),
			STATIC_READ(GL15.GL_STATIC_READ),
			DYNAMIC_DRAW(GL15.GL_DYNAMIC_DRAW),
			STREAM_DRAW(GL15.GL_STREAM_DRAW)
		;

		private int val;
		Usage(int val) {
			this.val = val;
		}
	}

	public enum Access {
		READ_ONLY(GL15.GL_READ_ONLY),
		WRITE_ONLY(GL15.GL_WRITE_ONLY),
		READ_WRITE(GL15.GL_READ_WRITE)
		;

		private int val;
		Access(int val) {
			this.val = val;
		}
	}

	public static int genBuffer() {
		return GL15.glGenBuffers();
	}

	public static void deleteBuffer(int buffer) {
		GL15.glDeleteBuffers(buffer);
	}

	public static void setBufferData(Target target, ByteBuffer buf, Usage usage) {
		if (GLWrapperSettings.doChecks) {

		}

		GL15.glBufferData(target.val, buf, usage.val);
	}

	public static void setBufferData(Target target, int size, Usage usage) {
		if (GLWrapperSettings.doChecks) {

		}

		GL15.glBufferData(target.val, size, usage.val);
	}

	public static ByteBuffer mapBuffer(Target target, Access access) {
		if (GLWrapperSettings.doChecks) {
			if (Buffer.getActive(target.val) == 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_OPERATION, "no buffer was bound to target");
				return null;
			}
		}

		return GL15.glMapBuffer(target.val, access.val, null);
	}

	public static void unmapBuffer(Target target) {
		if (GLWrapperSettings.doChecks) {
			if (Buffer.getActive(target.val) == 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_OPERATION, "no buffer was bound to target");
				return;
			}
		}

		GL15.glUnmapBuffer(target.val);
	}

	public static void bind(Target target, int buffer) {
		if (GLWrapperSettings.doChecks) {
			if (buffer == 0 && target != Target.PIXEL_UNPACK_BUFFER) {
				ExceptionHandler.handle(GLException.WARNING, "tried to bind 0 to buffer");
				// we don't return here because this is technically not an error
			}
		}

		Buffer.bind(target.val, buffer);
	}
}
