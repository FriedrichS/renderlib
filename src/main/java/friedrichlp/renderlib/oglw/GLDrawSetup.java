/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.oglw;

import friedrichlp.renderlib.library.GLException;
import friedrichlp.renderlib.oglw.state.VertexAttribArray;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL33;

public class GLDrawSetup {
	public enum Type {
		BYTE(GL11.GL_BYTE),
		UNSIGNED_BYTE(GL11.GL_UNSIGNED_BYTE),
		SHORT(GL11.GL_SHORT),
		UNSIGNED_SHORT(GL11.GL_UNSIGNED_SHORT),
		INT(GL11.GL_INT),
		UNSIGNED_INT(GL11.GL_UNSIGNED_INT),
		FLOAT(GL11.GL_FLOAT),
		DOUBLE(GL11.GL_DOUBLE),

		INT_2_10_10_10_REV(GL33.GL_INT_2_10_10_10_REV)
		;

		private int val;
		Type(int val) {
			this.val = val;
		}
	}

	public static void vertexAttribPointer(int index, int size, Type type, boolean normalized, int stride, int offset) {
		if (GLWrapperSettings.doChecks) {
			if (size < 1 || size > 4) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "size (%s) was outside the valid range (1, 2, 3, 4)", size);
				return;
			} else if (stride < 0) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "stride (%s) was negative", stride);
				return;
			} else if (type == Type.INT_2_10_10_10_REV && size != 4) {
				ExceptionHandler.handle(GLException.GL_INVALID_VALUE, "size (%s) was not 4 for INT_2_10_10_10_REV", size);
				return;
			}
		}

		if (!VertexAttribArray.isActive(index)) {
			VertexAttribArray.enable(index);
		}

		GL20.glVertexAttribPointer(index, size, type.val, normalized, stride, offset);
	}

	public static void vertexAttribDivisor(int index, int divisor) {
		GL33.glVertexAttribDivisor(index, divisor);
	}
}
