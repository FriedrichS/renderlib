/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.tracking;

import friedrichlp.renderlib.math.HitBox3;
import friedrichlp.renderlib.math.Matrix4f;
import friedrichlp.renderlib.math.Vector3;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.HackUtil;
import friedrichlp.renderlib.util.PartProperty;
import friedrichlp.renderlib.util.ValidationUtil;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectSet;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RenderObject {
	protected Vector3 position = Vector3.ZERO();
	protected Vector3 rotation = Vector3.ZERO();
	protected Vector3 scale = Vector3.ONE();

	protected Matrix4f modelTransform = new Matrix4f();
	protected boolean isDynamic;
	protected boolean isVisible; // visible on screen
	protected boolean isHidden; // if it should be hidden or not
	protected Model model;

	private Object2ObjectArrayMap<String, PartProperty> partOverrides = new Object2ObjectArrayMap<>();
	private RenderLayer layer;
	private boolean changed = false;

	protected RenderObject(Model model, RenderLayer layer) {
		this.model = model;
		this.layer = layer;
	}

	public Vector3 getPosition() {
		return position;
	}

	public Vector3 getRotation() {
		return rotation;
	}

	public Vector3 getScale() {
		return scale;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public boolean isHidden() {
		return isHidden;
	}

	public void setHidden(boolean hidden) {
		isHidden = hidden;
		layer.onObjectUpdate(this);
	}

	public void translate(float x, float y, float z) {
		synchronized (position) {
			position.add(x, y, z);
		}
		changed = true;
		layer.onObjectTransformUpdate(this);
	}

	public void rotate(float x, float y, float z) {
		synchronized (rotation) {
			rotation.add(x, y, z);
		}
		changed = true;
		layer.onObjectTransformUpdate(this);
	}

	public void scale(float x, float y, float z) {
		synchronized (scale) {
			scale.mul(x, y, z);
		}
		changed = true;
		layer.onObjectTransformUpdate(this);
	}

	public void translate(Vector3 v) {
		if (ValidationUtil.isNull(v, () -> ConsoleLogger.warn("position input was null!"))) return;
		translate(v.x, v.y, v.z);
	}

	public void rotate(Vector3 v) {
		if (ValidationUtil.isNull(v, () -> ConsoleLogger.warn("rotation input was null!"))) return;
		rotate(v.x, v.y, v.z);
	}

	public void scale(Vector3 v) {
		if (ValidationUtil.isNull(v, () -> ConsoleLogger.warn("scale input was null!"))) return;
		scale(v.x, v.y, v.z);
	}

	public void setPosition(float x, float y, float z) {
		synchronized (position) {
			position.set(x, y, z);
		}
		changed = true;
		layer.onObjectTransformUpdate(this);
	}

	public void setRotation(float x, float y, float z) {
		synchronized (rotation) {
			rotation.set(x, y, z);
		}
		changed = true;
		layer.onObjectTransformUpdate(this);
	}

	public void setScale(float x, float y, float z) {
		synchronized (scale) {
			scale.set(x, y, z);
		}
		changed = true;
		layer.onObjectTransformUpdate(this);
	}

	public void setPosition(Vector3 v) {
		if (ValidationUtil.isNull(v, () -> ConsoleLogger.warn("position input was null!"))) return;
		setPosition(v.x, v.y, v.z);
	}

	public void setRotation(Vector3 v) {
		if (ValidationUtil.isNull(v, () -> ConsoleLogger.warn("rotation input was null!"))) return;
		setRotation(v.x, v.y, v.z);
	}

	public void setScale(Vector3 v) {
		if (ValidationUtil.isNull(v, () -> ConsoleLogger.warn("scale input was null!"))) return;
		setScale(v.x, v.y, v.z);
	}

	public void resetPartOverrides() {
		if (isDynamic) layer.setDefault(this);
		isDynamic = false;
		layer.onObjectUpdate(this);

		partOverrides.values().forEach(PartProperty::resetTransform);
	}

	public void resetTransformSoft() {
		position.setZero();
		rotation.setZero();
		scale.setZero();

		changed = true;
		layer.onObjectTransformUpdate(this);
	}

	public void resetTransformHard() {
		resetTransformSoft();
		resetPartOverrides();
	}

	public void translatePart(String partName, float x, float y, float z) {
		getPart(partName).translate(x, y, z);
	}

	public void rotatePart(String partName, float x, float y, float z) {
		getPart(partName).rotate(x, y, z);
	}

	/*public void rotatePart(String partName, float x, float y, float z, Vector3 origin){
		PartProperty part = partOverrides.get(partName);
		if (part == null) {
			part = new PartProperty();
			partOverrides.put(partName, part);
		}
		part.rotate(x, y, z, origin);

		if(!isDynamic) layer.setDynamic(this);
		isDynamic = true;
		layer.onObjectUpdate(this);
	}*/

	public void scalePart(String partName, float x, float y, float z) {
		getPart(partName).scale(x, y, z);
	}

	public void setPartPosition(String partName, float x, float y, float z) {
		getPart(partName).setPosition(x, y, z);
	}

	public void setPartRotation(String partName, float x, float y, float z) {
		getPart(partName).setRotation(x, y, z);
	}

	public void setPartScale(String partName, float x, float y, float z) {
		getPart(partName).setScale(x, y, z);
	}

	public void setPartOrigin(String partName, float x, float y, float z) {
		getPart(partName).setOrigin(x, y, z);
	}

	public void setPartHidden(String partName, boolean hidden) {
		getPart(partName).setHidden(hidden);
	}

	// hook for changing part transform on the fly
	public PartProperty getPart(String partName) {
		PartProperty part = partOverrides.get(partName);
		if (part == null) {
			part = new PartProperty();
			partOverrides.put(partName, part);
		}

		if (!isDynamic) layer.setDynamic(this);
		isDynamic = true;
		layer.onObjectUpdate(this);

		return part;
	}

	public ObjectSet<String> getParts() {
		return model.groups.keySet();
	}

	public HitBox3 getHitbox() {
		return model.getHitbox();
	}

	public void renderSingle() {
		TrackingManager.state.save();

		Model.globalRenderSetup();
		model.renderSetup(false);
		render();
		model.renderCleanup();
		Model.globalRenderCleanup();

		TrackingManager.state.restore();
	}

	protected void render() {
		model.renderSingle(modelTransform, partOverrides);
	}

	private void updateTransform() {
		synchronized (modelTransform) {
			modelTransform.setIdentity();
			modelTransform.multiply(Matrix4f.SCALE(scale.x, scale.y, scale.z));
			if (rotation.x != 0) modelTransform.multiply(Matrix4f.TEMPORARY().rotate(rotation.x, 1, 0, 0));
			if (rotation.y != 0) modelTransform.multiply(Matrix4f.TEMPORARY().rotate(rotation.y, 0, 1, 0));
			if (rotation.z != 0) modelTransform.multiply(Matrix4f.TEMPORARY().rotate(rotation.z, 0, 0, 1));
			modelTransform.multiply(Matrix4f.TRANSLATE(position.x, position.y, position.z));
		}
		layer.onObjectUpdate(this);
	}

	protected void applyTransformUpdates() {
		if (!changed) return;
		changed = false;

		updateTransform();

		Object[] values = HackUtil.getValues(partOverrides);
		for (int i = 0; i < partOverrides.size(); i++) {
			((PartProperty) values[i]).applyTransformUpdates();
		}
	}

	protected static class ProcessingBatch implements Runnable {
		private static Queue<Object[]> unusedArrays = new LinkedList<Object[]>();

		private static Object[] get() {
			synchronized (unusedArrays) {
				if (!unusedArrays.isEmpty()) return unusedArrays.poll();
				return new Object[RenderLayer.PROCESSING_BATCH_SIZE];
			}
		}

		private Object[] objects;
		private int size;

		protected ProcessingBatch(ObjectArrayList<RenderObject> l, int idx, int size) {
			this.size = size;
			objects = get();
			System.arraycopy(HackUtil.getInternalArray(l), idx, objects, 0, size);
		}

		@Override
		public void run() {
			for (int i = 0; i < size; i++) {
				((RenderObject) objects[i]).applyTransformUpdates();
				objects[i] = null;
			}

			synchronized (unusedArrays) {
				unusedArrays.add(objects);
			}
		}
	}
}
