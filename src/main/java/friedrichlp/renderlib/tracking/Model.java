/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.tracking;

import friedrichlp.renderlib.RenderLibSettings;
import friedrichlp.renderlib.caching.CacheManager;
import friedrichlp.renderlib.caching.util.SerializeField;
import friedrichlp.renderlib.library.*;
import friedrichlp.renderlib.math.HitBox3;
import friedrichlp.renderlib.math.Matrix4f;
import friedrichlp.renderlib.math.Vector2;
import friedrichlp.renderlib.math.Vector3;
import friedrichlp.renderlib.model.*;
import friedrichlp.renderlib.oglw.GLBuffers;
import friedrichlp.renderlib.oglw.GLDraw;
import friedrichlp.renderlib.oglw.GLDrawSetup;
import friedrichlp.renderlib.render.GLState;
import friedrichlp.renderlib.render.ShaderProgram;
import friedrichlp.renderlib.render.VertexArray;
import friedrichlp.renderlib.threading.ModelLoader;
import friedrichlp.renderlib.threading.TaskManager;
import friedrichlp.renderlib.util.*;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Model extends DynamicLoader {
	private static final int VBO_CELL_SIZE = 12 + 4 + 8;
	private static final ShaderProgram RENDER_SHADER = new ShaderProgram("/assets/shaders/ModelTexture.vert", "/assets/shaders/ModelTexture.frag");
	private static int vbosLoading;
	private static Matrix4f transformMatrixBuffer = new Matrix4f();
	private static FloatBuffer transformBuffer = transformMatrixBuffer.getAsBuffer();
	private static GLState vboState = new GLState().add(GLValueType.ARRAY_BUFFER);

	public final ModelLoaderProperty properties;
	public Object2ObjectArrayMap<String, int[]> groups = new Object2ObjectArrayMap<>();
	public MaterialCollection materials;
	public final String identifier;
	public final ModelData data;
	public final int id;
	public ObjectArrayList<QuadConsumer<FloatArrayList, FloatArrayList, FloatArrayList, IntArrayList>> meshHooks = new ObjectArrayList<>();
	public Runnable onLoad;

	protected int renderEffects;

	private ModelType type;
	private IFileContainer file;
	private TextureCollection textures;
	private ModelLoadState loadState = ModelLoadState.UNLOADED;
	private VertexArray vbo = new VertexArray(GLBuffers.Target.VERTEX_ARRAY);
	private ShaderProgram.SubShader renderShader;
	private ShaderProgram.SubShader renderShaderInstanced;

	protected Model(IFileContainer file, ModelLoaderProperty properties, String identifier, int id) {
		this.identifier = identifier;
		String path = file.getPath();
		type = ModelType.fromFileExtension(path.substring(path.lastIndexOf(".") + 1));
		if (type == null) {
			throw new IllegalArgumentException("The given model file is not in a supported format (" + path.substring(path.lastIndexOf(".") + 1) + ")");
		}

		this.file = file;
		this.properties = properties;
		this.id = id;

		data = new ModelData(this);
		CacheManager.link(data);

		textures = new TextureCollection(this);
	}

	public IFileContainer getFile() {
		return file;
	}

	public HitBox3 getHitbox() {
		return data.hitbox;
	}

	public void setHitbox(HitBox3 hitbox) {
		data.hitbox = hitbox;
		data.onChange();
	}

	public ModelType getModelType() {
		return type;
	}

	public boolean isLoaded() {
		return loadState == ModelLoadState.LOADED;
	}

	public boolean isLoading() {
		return loadState == ModelLoadState.LOADING;
	}

	public boolean isUnloaded() {
		return loadState == ModelLoadState.UNLOADED;
	}

	// gets called multithreaded
	private IntArrayList faceVerts;
	public void setData(FloatArrayList verts, FloatArrayList vertNorms, FloatArrayList vertTex, IntArrayList faceVerts, ObjectArrayList<String> faceMTLs) {
		data.vboSize = 0;
		for (int[] faces : groups.values()) data.vboSize += faces.length;

		if (TrackingManager.useGL()) {
			int cellSize = VBO_CELL_SIZE;
			for (TextureType t : TextureType.values()) {
				if (t.isActive(data.loadedTextureTypes)) {
					cellSize += t.getSize();
				}
			}
			data.modelBuf = BufferUtils.createByteBuffer(data.vboSize * 3 * cellSize);
			this.faceVerts = faceVerts;
			Vector3[] vBuf = new Vector3[3];
			Vector3[] vnBuf = new Vector3[3];
			Vector2[] vtBuf = new Vector2[3];
			int idx = 0;
			Group prevGroup = null;
			HitBox3 hitbox = new HitBox3();
			for (Map.Entry<String, int[]> e : groups.entrySet()) {
				if (prevGroup != null) prevGroup.setEnd(idx);
				Group group = new Group(idx);
				data.groups.put(e.getKey(), group);
				prevGroup = group;
				HitBox3 box = new HitBox3();

				for (int face : e.getValue()) {
					int[][] points = facePoints(face);
					for (int i = 0; i < 3; i++) {
						int vi = points[i][0] * 3;
						int vti = points[i][1] * 2;
						int vni = points[i][2] * 3;

						Vector3 v = Vector3.TEMPORARY(verts.getFloat(vi), verts.getFloat(vi + 1), verts.getFloat(vi + 2));
						vBuf[i] = v;
						box.minX = Math.min(box.minX, v.x);
						box.minY = Math.min(box.minY, v.y);
						box.minZ = Math.min(box.minZ, v.z);
						box.maxX = Math.max(box.maxX, v.x);
						box.maxY = Math.max(box.maxY, v.y);
						box.maxZ = Math.max(box.maxZ, v.z);

						if (vni != -3) {
							vnBuf[i] = Vector3.TEMPORARY(vertNorms.getFloat(vni), vertNorms.getFloat(vni + 1), vertNorms.getFloat(vni + 2));
						} else {
							vnBuf[i] = Vector3.TEMPORARY();
						}
						if (vti != -2) {
							vtBuf[i] = Vector3.TEMPORARY(vertTex.getFloat(vti), vertTex.getFloat(vti + 1));
						} else {
							vtBuf[i] = Vector2.TEMPORARY();
						}
					}

					Vector3 tangent = null;
					Vector3 biTangent = null;
					if (TextureType.MAP_BUMP.isActive(data.loadedTextureTypes)) {
						Vector3 dPos1 = Vector3.TEMPORARY(vBuf[1]).sub(vBuf[0]);
						Vector3 dPos2 = Vector3.TEMPORARY(vBuf[2]).sub(vBuf[0]);
						Vector2 dUV1 = Vector2.TEMPORARY(vtBuf[1]).sub(vtBuf[0]);
						Vector2 dUV2 = Vector2.TEMPORARY(vtBuf[2]).sub(vtBuf[0]);

						float r = 1.0f / (dUV1.x * dUV2.y - dUV1.y * dUV2.x);
						tangent = Vector3.TEMPORARY(dPos1).mul(dUV2.y).sub(Vector3.TEMPORARY(dPos2).mul(dUV1.y)).mul(r);
						biTangent = Vector3.TEMPORARY(dPos2).mul(dUV1.x).sub(Vector3.TEMPORARY(dPos1).mul(dUV2.x)).mul(r);

						for (int i = 0; i < 3; i++) {
							// orthogonalize
							float nDotT = Vector3.TEMPORARY(vnBuf[i]).dot(tangent);
							tangent.sub(Vector3.TEMPORARY(vnBuf[i]).mul(nDotT)).normalize();

							if (Vector3.TEMPORARY(vnBuf[i]).cross(tangent).dot(biTangent) < 0.0f) {
								tangent.mul(-1);
							}
						}
					}

					for (int i = 0; i < 3; i++) {
						Vector3 v = vBuf[i];
						data.modelBuf.putFloat(v.x);
						data.modelBuf.putFloat(v.y);
						data.modelBuf.putFloat(v.z);

						data.modelBuf.putInt(packVector(vnBuf[i]));

						Vector2 vt = vtBuf[i];
						data.modelBuf.putFloat(vt.x);
						data.modelBuf.putFloat(vt.y);

						if (tangent != null) {
							data.modelBuf.putInt(packVector(tangent));
							data.modelBuf.putInt(packVector(biTangent));
						}
					}

					idx += 3;
				}

				group.hitbox = box;
				hitbox.combine(box);
			}
			data.modelBuf.flip();

			setHitbox(hitbox);
		} else {
			int idx = 0;
			String prevGroup = null;
			for (Map.Entry<String, int[]> e : groups.entrySet()) {
				if (prevGroup != null) data.groups.get(prevGroup).setEnd(idx);
				data.groups.put(e.getKey(), new Group(idx));
				prevGroup = e.getKey();
				idx += e.getValue().length * 3;
			}
		}
		this.faceVerts = null;

		// ToDo: set hitbox

		if (onLoad != null) {
			onLoad.run();
			onLoad = null;
		}

		for (QuadConsumer<FloatArrayList, FloatArrayList, FloatArrayList, IntArrayList> hook : meshHooks) {
			hook.accept(verts, vertNorms, vertTex, faceVerts);
		}

		data.loadedTextureTypes = materials.loadedTextureTypes;

		if (TrackingManager.useGL()) {
			textures.loadFromFile(faceMTLs);
		}

		data.onChange();
		TaskManager.scheduleTask(this::finishLoading);
	}


	private int packVector(Vector3 v) {
		int xs = v.x < 0 ? 1 : 0;
		int ys = v.y < 0 ? 1 : 0;
		int zs = v.z < 0 ? 1 : 0;

		return zs << 29 | ((int) (v.z * 511 + (zs << 9)) & 511) << 20 |
				ys << 19 | ((int) (v.y * 511 + (ys << 9)) & 511) << 10 |
				xs << 9 | ((int) (v.x * 511 + (xs << 9)) & 511);
	}

	private int[][] facePoints(int start) {
		start *= 9;
		int[][] points = new int[3][];
		for (int i = 0; i < 3; i++) {
			points[i] = new int[]{faceVerts.getInt(start + i * 3 + 0), faceVerts.getInt(start + i * 3 + 1), faceVerts.getInt(start + i * 3 + 2)};
		}
		return points;
	}

	public void render(int count, TransformStack transform) {
		if (count == 0) return;
		if (!tryCreateVbo()) return;
		onUse();

		renderSetup(true);
		transform.bind();

		GLDraw.drawArraysInstanced(GLDraw.Mode.TRIANGLES, 0, data.vboSize * 3, count);

		renderCleanup();
	}

	public void renderSingle(Matrix4f modelTransform, Map<String, PartProperty> partOverrides) {
		if (!tryCreateVbo()) return;
		onUse();

		for (Map.Entry<String, Group> e : data.groups.entrySet()) {
			PartProperty part = partOverrides.get(e.getKey());
			if (part != null) {
				if (!part.isHidden()) {
					transformMatrixBuffer
							.copyFrom(part.getTransformMatrix())
							.multiply(modelTransform)
							.writeToBuffer(transformBuffer);
					renderShader.setMatrix("uniformModelTransform", transformBuffer);
					GLDraw.drawArrays(GLDraw.Mode.TRIANGLES, e.getValue().start, e.getValue().length);
					renderShader.setMatrix("uniformModelTransform", modelTransform);
				}
			} else {
				GLDraw.drawArrays(GLDraw.Mode.TRIANGLES, e.getValue().start, e.getValue().length);
			}
		}
	}

	// runs on GL thread
	public void loadFromCache() {
		textures.loadFromCache();
	}

	private boolean tryCreateVbo() {
		if (isLoaded()) return true;
		if (isLoading()) return false;
		if (vbosLoading >= RenderLibSettings.General.LOADING_MODE.vboLoadLimit) return false;

		if (isUnloaded()) {
			load();
		}

		return false;
	}

	public void finishLoading() {
		// will always return true on the server since no textures are present
		if (!textures.isLoaded()) {
			return;
		}

		if (textures.hasError()) {
			loadState = ModelLoadState.LOADED;
			data.modelBuf = null;
			vbosLoading--;
			return;
		}

		if (TrackingManager.useGL()) {
			vbo.set(data.modelBuf, () -> {
				if (data.needsDataRefresh) TaskManager.runTaskAsync(() -> data.writeCache());
				else data.clearBuffers();
				loadState = ModelLoadState.LOADED;
				vbosLoading--;
				onUse();
			});
		}
	}

	@Override
	public void onLoad() {
		vbosLoading++;
		ConsoleLogger.debug("Loading model %s", file.getName());

		loadState = ModelLoadState.LOADING;
		ModelLoader loader = new ModelLoader(this, file);
		TaskManager.runTaskAsync(loader);
	}

	@Override
	public void onUnload() {
		ConsoleLogger.debug("Unloading model %s", file.getName());

		if (TrackingManager.useGL()) {
			textures.unload();
			vbo.delete();
		}

		loadState = ModelLoadState.UNLOADED;
	}

	public boolean renderSetup(boolean instancedDraw) {
		if (!isLoaded()) return false;
		if (!textures.isLoaded()) return false;

		shaderSetup(instancedDraw);

		int cellSize = VBO_CELL_SIZE;
		for (TextureType t : TextureType.values()) {
			if (t.isActive(data.loadedTextureTypes)) {
				cellSize += t.getSize();
			}
		}

		textures.renderSetup();

		vbo.bind();
		GLDrawSetup.vertexAttribPointer(5, 3, GLDrawSetup.Type.FLOAT, false, VBO_CELL_SIZE, 0);
		GLDrawSetup.vertexAttribPointer(6, 4, GLDrawSetup.Type.INT_2_10_10_10_REV, true, VBO_CELL_SIZE, 12);
		GLDrawSetup.vertexAttribPointer(7, 2, GLDrawSetup.Type.FLOAT, false, VBO_CELL_SIZE, 16);

		AtomicInteger idx = new AtomicInteger(VBO_CELL_SIZE);
		for (TextureType t : TextureType.values()) {
			if (t.isActive(data.loadedTextureTypes)) {
				t.renderSetup(idx, cellSize);
			}
		}

		return true;
	}

	public void renderCleanup() {
		if (!isLoaded()) return;
		if (!textures.isLoaded()) return;

		for (TextureType t : TextureType.values()) {
			if (t.isActive(data.loadedTextureTypes)) {
				t.renderCleanup();
			}
		}
	}

	private void shaderSetup(boolean instancedDraw) {
		if (renderShader == null) {
			int renderProperties = 0;
			int code = data.loadedTextureTypes;
			if (TextureType.MAP_BUMP.isActive(code)) renderProperties |= RenderProperty.NORMAL_MAP.get();
			if (TextureType.REFL.isActive(code)) renderProperties |= RenderProperty.METALLIC_MAP.get();

			renderShader = RENDER_SHADER.get(renderEffects, renderProperties);
			renderShaderInstanced = RENDER_SHADER.get(renderEffects, renderProperties | RenderProperty.INSTANCED_DRAW.get());
		}

		ShaderProgram.SubShader shader = instancedDraw ? renderShaderInstanced : renderShader;
		shader.bind();
		shader.bindUniformBlock(textures.textureMtlBuf.getId(), "MtlUvBlock", 0);

		shader.setMatrix("modelViewProjection", TrackingManager.modelViewProjectionMatrix);
		shader.setMatrix("modelView", TrackingManager.modelViewMatrix);
		shader.setVector("camPos", Vector3.TEMPORARY(TrackingManager.getCameraPos()).add(0, RenderLibSettings.Rendering.CAMERA_HEIGHT_OFFSET, 0));
		shader.setFloat("ambientLightStrength", RenderLibSettings.Rendering.AMBIENT_LIGHT_STRENGTH);
		shader.setFloat("bumpStrength", 1);
	}


	protected static void globalRenderSetup() {
		GLValueType.CULL_FACE.set(false);
	}

	protected static void globalRenderCleanup() {

	}

	public static class Group {
		@SerializeField
		public int start;
		@SerializeField
		public int length;
		@SerializeField
		public HitBox3 hitbox;

		public Group() {}

		public Group(int start) {
			this.start = start;
		}

		private void setEnd(int idx) {
			length = idx - start;
		}
	}
}