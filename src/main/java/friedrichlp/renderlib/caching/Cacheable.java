/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.caching;

import net.openhft.hashing.LongHashFunction;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Cacheable {
	private long nameHash;

	public void setName(String name) {
		nameHash = LongHashFunction.wy_3().hashChars(name);
	}


	public long getNameHash() {
		return nameHash;
	}

	public String getNameHashString() {
		return String.format("%016X", nameHash);
	}

	public void onChange() {
		CacheManager.needsSave = true;
	}

	public void onSave(DataOutputStream out) throws IOException {}

	public void onLoad(DataInputStream in) throws IOException {}
}
