/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.caching.util;

import friedrichlp.renderlib.RenderLibRegistry;
import friedrichlp.renderlib.util.IFileContainer;
import friedrichlp.renderlib.util.UnsafeUtil;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class DataSerializer {
	private DataInputStream in;
	private DataOutputStream out;

	public DataSerializer(DataInputStream in) {
		this.in = in;
	}

	public DataSerializer(DataOutputStream out) {
		this.out = out;
	}

	public void writeHashedFile(HashedFile f) throws IOException {
		writeFileContainer(f.file);
		out.writeInt(f.size);
		out.writeInt(f.blockSize);
		out.writeInt(f.checksums.length);
		for (int i = 0; i < f.checksums.length; i++) {
			out.writeLong(f.checksums[i]);
		}
	}

	public HashedFile readHashedFile() throws IOException {
		HashedFile f = new HashedFile();

		f.file = readFileContainer();
		f.size = in.readInt();
		f.blockSize = in.readInt();
		f.checksums = new long[in.readInt()];
		for (int i = 0; i < f.checksums.length; i++) {
			f.checksums[i] = in.readLong();
		}

		return f;
	}

	public void writeFileContainer(IFileContainer fc) throws IOException {
		out.writeInt(RenderLibRegistry.FileContainer.indexOf(fc.getClass()));
		fc.save(out);
	}

	public IFileContainer readFileContainer() throws IOException {
		IFileContainer fc = (IFileContainer) UnsafeUtil.createInstance(RenderLibRegistry.FileContainer.get(in.readInt()), false);
		fc.load(in);
		return fc;
	}
}
