/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.threading;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskManager {
	private static ObjectArrayList<Runnable> tasks = new ObjectArrayList<Runnable>();
	private static ExecutorService executor = Executors.newFixedThreadPool(4);

	public static void runTaskAsync(Runnable task) {
		executor.execute(task);
	}

	public static void scheduleTask(Runnable task) {
		synchronized (tasks) {
			tasks.add(task);
		}
	}

	public static void runScheduledTasks() {
		synchronized (tasks) {
			tasks.forEach(Runnable::run);
			tasks.clear();
		}
	}
}
